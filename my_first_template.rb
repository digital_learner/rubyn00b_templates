# Railify - Digital Learning Journey's Rails Template

author = "RubyN00b"

@install_options = {}

def install_options
  @install_options
end

@configure_app_options = {}

def configure_app_options
  @configure_app_options
end

def replace_line(path, options = {})
  lines = File.open(path).readlines
  lines.map! do |line|
    if line.match(options[:match])
      line = "#{options[:with].rstrip}\n"
    end
    line
  end 

  run "rm #{path}"
  File.open(path, 'w+') { |file| file << lines.join }
end

def ask_with_default(prompt, default)
  value = ask("#{prompt} [#{default}]")
  value.blank? ? default : value
end

def get_rid_of_shitty_double_quotes
  require 'find'
  Find.find('.').each do |file|
    if file.match(/\.rb$/)
     lines = File.open(file).readlines
     lines.map! { |line| line.gsub(/"/, "'") }
     file = File.open(file, 'w+')
     file << lines.join
     file.close
    end
  end
end

def install_additional_gems
  gems = %w{easy_auth}.inject([]) do |list, gem|
    list << install_options[gem.to_sym].gemfile if install_options[gem.to_sym]
    list
  end

  unless gems.empty?
    gems.join("\n") + "\n"
  end
end

def install_os_dependent_gems
  # OS dependent - based on 2nd Ed of MHartl 'Rails Tutorial'
  gems = []

  case  `uname -a | cut -f1`
    when /Darwin/i
      host_os = "Mac OSX"
      gems << "  # System-dependent gems (Mac OSX)"
      gems << "  gem 'rb-fsevent', '0.9.1', :require => false"
      gems << "  gem 'growl', '1.0.3'"
    when /linux|arch/i
      host_os = "Linux"
      gems << "  # System-dependent gems (Linux)"
      gems << "  gem 'rb-notify', '0.8.8'"
      gems << "  gem 'libnotify', '0.5.9'"
    when /sunos|solaris/i
      host_os = "Solaris"
      gems << "  # System-dependent gems (Solaris)"
    when /mswin|windows/i
      host_os = "Windows"
      gems << "  # System-dependent gems (Windows)"
      gems << "  gem 'rb-fchange', '0.0.5'"
      gems << "  gem 'rb-notifu', '0.0.4'"
      gems << "  gem 'wind32console', '1.3.0'"
    else
      puts "Unknown OS - no additional gems to add"
  end 

  unless gems.empty?
    puts "Updating #{host_os} OS specific gem information for inclusion in Gemfile"
    gems.join("\n")
  end
end


# Use rvm for gemset mangement?
if yes?("Do you want to use rvm gemsets to isolate gems from other projects (RVM: #{`rvm current`.strip})?")
  install_options[:rvmrc] = true
end


# Gems
# if yes?('Install Backbone?')
#   install_options[:backbone] = true
# end

if yes?('Install Cucumber?')
  install_options[:cucumber] = true
end


# Database
if yes?('Database: SQLite?')
  configure_app_options[:database_sqlite3] = true
  if yes?('  - Development/Test Only - NOT Production?')
    configure_app_options[:database_sqlite3_IN_dev_test] = true
  else
    configure_app_options[:database_sqlite3_IN_dev_test_prod] = true
  end
end
if yes?('Database: Postgresql?')
  configure_app_options[:database_postgresql] = true
  if yes?('  - Production Only?')
    configure_app_options[:database_postgresql_IN_prod] = true
  else
    configure_app_options[:database_postgresql_IN_dev_test_prod] = true
  end
end


# Additional Build steps - based on sections of Tutorial
# if yes?('Base on bare-bones MHartl Rails Tutorial (Own Authentication, Sample App layout, BaseTitle, Basic Tests)?')
#   configure_app_options[:bare_bones_RailsTutorial] = true
# end

if yes?('Add static_pages (Home, About, Contact, Help)?')
  configure_app_options[:static_pages] = true
end

if yes?('Add base title?')
  configure_app_options[:base_title_helper] = true
end

if yes?('Add debug parameters to view (Development Only)?')
  configure_app_options[:show_debug_params_in_dev] = true
end

if yes?('Error Messages (Generate shared view)?')
  configure_app_options[:error_messages_shared_view] = true
end

if yes?('Sign-in/Out & Authentication')
  configure_app_options[:homespun_authentication] = true
end

if yes?('Testing: Include RSpec (from Tutorial)?')
  configure_app_options[:rspec_base] = true
end
# ===============================================  End of Questions =================================================== #


# StaticPages Build
# ===================================================================================================================== #
if configure_app_options[:static_pages]
  say "Generating Static Pages for your shiny new app!"

  # CONTROLLERS
  inside('app/controllers') do
  file 'static_pages_controller.rb', <<-CONTROLLER
class StaticPagesController < ApplicationController
  def home
  end

  def help
  end

  def about
  end

  def contact
  end
end
CONTROLLER
  end


  # VIEWS
  inside('app/views') do
    run 'mkdir static_pages'
    run 'mkdir shared'

    # file additions
    # layouts/shared
    file 'layouts/_header.html.erb', <<-VIEW
<header class="navbar navbar-fixed-top navbar-inverse">
  <div class="navbar-inner">
    <div class="container">
      <%= link_to "#{app_name}", root_path, id: "logo" %>
      <nav>
        <ul class="nav pull-right">
          <li><%= link_to "Home",    root_path %></li>
          <li><%= link_to "Help",    help_path %></li>
        </ul>
      </nav>
    </div>
  </div>
</header>
VIEW

    file 'layouts/_footer.html.erb', <<-VIEW
<footer class="footer">
  <small>
    <a href="#">#{app_name}</a>
    by #{author}
  </small>
  <nav>
    <ul>
      <li><%= link_to "About",   about_path %></li>
      <li><%= link_to "Contact", contact_path %></li>
      <li><a href="#">News</a></li>
    </ul>
  </nav>
</footer>
VIEW

    file 'layouts/_shim.html.erb', <<-VIEW
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
VIEW

    replace_line('layouts/application.html.erb', :match => /<%= yield %>\n/, :with => '')
    gsub_file('layouts/application.html.erb', /^[\s]*$\n/, "")
    insert_into_file 'layouts/application.html.erb', :after => %{<body>\n} do
      "  <%= render 'layouts/header' %>\n" +
      "  <div class = 'container'>\n" +
      "    <% flash.each do |key, value| %>\n" +
      '      <%= content_tag(:div, value, class: "alert alert-#{key}") %>' + "\n" +
      "    <% end %>\n" +
      "    <%= yield %>\n" +
      "    <%= render 'layouts/footer' %>\n" +
      "  </div>\n"
    end

    if configure_app_options[:show_debug_params_in_dev]
      insert_into_file 'layouts/application.html.erb', :after => %{<%= render 'layouts/footer' %>\n} do
        "      <%= debug(params) if Rails.env.development? %>\n"
      end
    end

    file 'static_pages/home.html.erb', <<-VIEW
<div class="center hero-unit">
  <h1>Welcome to #{app_name}</h1>
  <h2>
    This is the home page for the 
    <a hef="#">#{app_name}</a>.
  </h2>

</div>

<%= link_to image_tag("rails.png", alt: "Rails"), 'http://rubyonrails.org/' %>
VIEW

    file 'static_pages/about.html.erb', <<-VIEW
<% provide(:title, 'About Us') %>
<h1>About Us</h1>
<p>
  The <a href="#">#{app_name}</a>
  is a project by #{author} <%= "#{author}" == "RubyN00b" ? "(Mark)" : nil %> to blog about his code learning journey
  in the <strong>web space</strong>. He blogs mainly about Ruby, Rails, Javascript, HTML and jQuery.
</p>
VIEW

    file 'static_pages/contact.html.erb', <<-VIEW
<% provide(:title, 'Contact') %>
<h1>Contact</h1>
<p>
  Contact #{author} about the #{app_name} at the <a href="#">contact page</a>.
</p>
VIEW

    file 'static_pages/help.html.erb', <<-VIEW
<% provide(:title, 'Help') %>
<h1>Help</h1>
<p>
  Get help for #{app_name} right here.
  Please follow the links provided.
</p>
VIEW

  end

  # ASSETS/STYLESHEETS
  inside('app/assets/stylesheets') do
    file 'custom.css.scss', <<-STYLESHEET
@import "bootstrap";

/* mixins, variables, etc */

$grayMediumLight: #eaeaea;

@mixin box_sizing {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

/* universal */

html {
  overflow-y: scroll;
}

body {
  padding-top: 60px;
}

section {
  overflow: auto;
}

textarea {
  resize: vertical;
}

.center {
  text-align: center;
  h1 {
    margin-bottom: 10px;
  }
}


/* typography */

h1, h2, h3, h4, h5, h6 {
  line-height: 1;
}

h1 {
  font-size: 3em;
  letter-spacing: -2px;
  margin-bottom: 30px;
  text-align: center;
}

h2 {
  font-size: 1.7em;
  letter-spacing: -1px;
  margin-bottom: 30px;
  text-align: center;
  font-weight: normal;
  color: $grayLight
}

p {
  font-size: 1.1em;
  line-height: 1.7em;
}

/* header */

#logo {
  float: left;
  margin-right: 10px;
  font-size: 1.7em;
  color: #fff;
  text-transform: uppercase;
  padding-top: 9px;
  font-weight: bold;
  line-height: 1;
  &:hover {
    color: #fff;
    text-decoration: none;
  }
}

/* footer */

footer {
  margin-top: 45px;
  padding-top: 5px;
  border-top: 1px solid $grayMediumLight;
  color: $grayLight;
  a {
    color: $gray;
    &:hover {
      color: $grayDarker;
    }
  }
  small {
    float: left;
  }
  ul {
    float: right;
    list-style: none;
    li {
      float: left;
      margin-left: 10px;
    }
  }
}

/* sidebar */

aside {
  section {
    padding: 10px 0;
    border-top: 1px solid $grayLighter;
    &:first-child {
      border: 0;
      padding-top: 0;
    }
    span {
      display: block;
      margin-bottom: 3px;
      line-height: 1;
    }
    h1 {
      font-size: 1.4em;
      text-align: left;
      letter-spacing: -1px;
      margin-bottom: 3px;
      margin-top: 0px;
    }
  }
}

.gravatar {
  float: left;
  margin-right: 10px;
}


/* forms */

input, textarea, select, .uneditable-input {
  border: 1px solid #bbb;
  width: 100%;
  padding: 10px;
  margin-bottom: 15px;
  @include box_sizing;
}

input {
  height: auto !important;
}

#error_explanation {
  color: #f00;
  ul {
    list-style: none;
    margin: 0 0 18px 0;
  }
}

.field_with_errors {
  @extend .control-group;
  @extend .error;
}
/* miscellaneous */

.debug_dump {
  clear: both;
  float: left;
  width: 100%;
  margin-top: 45px;
  @include box_sizing;
}

/* users index */

.users {
  list-style: none;
  margin: 0;
  li {
    overflow: auto;
    padding: 10px 0;
    border-top: 1px solid $grayLighter;
    &:last-child {
      bottom-border: 1px solid $grayLighter;
    }
  }
}
STYLESHEET
  end


  # ASSETS/JAVASCRIPTS
  inside('app/assets/javascripts') do
    insert_into_file 'application.js', :before => %{//= require_tree .\n} do
      "//= require bootstrap\n"
    end
  end


  # ROUTES
  file 'config/routes.rb', <<-ROUTES, :force => true
#{app_name.classify}::Application.routes.draw do
  #root :to => 'landing#show'
end
  ROUTES

  if configure_app_options[:static_pages]
    insert_into_file 'config/routes.rb', :after => /routes.draw do\n/ do

      "  match '/signup',  to:  'users#new'\n" +
      "  match '/signin',  to:  'sessions#new'\n" +
      "  match '/signout', to:  'sessions#destroy', via: :delete\n" +
      "\n" +
      "  match '/help',    to:  'static_pages#help'\n" +
      "  match '/about',   to:  'static_pages#about'\n" +
      "  match '/contact', to:  'static_pages#contact'\n" +
      "\n" +
      "  root :to => 'static_pages#home'\n"
    end
    gsub_file('config/routes.rb', /#root :to => 'landing#show'/, '')
  end
end # StaticPages BUILD COMPLETE


# BaseTitleHelper BUILD
# ===================================================================================================================== #

if configure_app_options[:base_title_helper]
  # Views
  inside('app/views') do
    gsub_file('layouts/application.html.erb', /<title>#{app_name.humanize}<\/title>/, "  <title><%= full_title(yield(:title)) %></title>")
  end

  # Helpers
  inside('app/helpers') do
    insert_into_file 'application_helper.rb', :before => /end/ do

      "\n  def full_title(page_title)\n" +
      "    base_title = '#{app_name}'\n" +
      "    if page_title.empty?\n" +
      "      base_title\n" +
      "    else\n" +
      '      "#{base_title} | #{page_title}"' + "\n" +
      "    end\n" +
      "  end\n"
    end
  end
end # BaseTitleHelper BUILD COMPLETE


# ErrorMessagesSharedView BUILD
# ===================================================================================================================== #
if configure_app_options[:error_messages_shared_view]
  # VIEWS
  inside('app/views') do
    file 'shared/_error_messages.html.erb', <<-VIEW
<% if object.errors.any? %>
  <div id="error_explanation">
    <div class="alert alert-error">
      The form contains <%= pluralize(object.errors.count, "error") %>.
    </div>
    <ul>
      <% object.errors.full_messages.each do |msg| %>
        <li>* <%=msg %></li>
      <% end %>
    </ul>
  </div>
<% end %>
VIEW
  end
  puts "Usage: (Views) => render 'shared/error_messages' <= "
end # ErrorMessagesSharedView BUILD COMPLETE


# Homespun Authentication Build
# ===================================================================================================================== #
if configure_app_options[:homespun_authentication]
  say "Adding Signin/Signout and Authentication functionality - Based on MHartl tutorial"

  # MIGRATIONS
  inside('db/migrate') do
    file '01_create_users.rb', <<-DB_MIGRATION
class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest      
      t.string :remember_token
      t.boolean :admin, default: false

      t.timestamps
    end
    add_index :users, :email, unique: true
    add_index  :users, :remember_token    
  end
end    
DB_MIGRATION
  end

  # MODELS
  # reminder escape '\'s with '\\'
  # (in templates where we want a literal \ in our script to escape it with \)
  inside('app/models') do
    file 'user.rb', <<-MODEL
# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  attr_accessible :email, :name, :password, :password_confirmation
  has_secure_password

  #before_save { |user| user.email = email.downcase }
  before_save { |user| self.email.downcase! }
  before_save :create_remember_token 

  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\\A[\\w+\\-.]+@[a-z\\d\\-.]+\\.[a-z]+\\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }
  validates :password_confirmation, presence: true

  private

    def create_remember_token
      self.remember_token = SecureRandom.urlsafe_base64
    end
end

MODEL
  end

  # CONTROLLERS
  inside('app/controllers') do
    file 'users_controller.rb', <<-CONTROLLER
class UsersController < ApplicationController
  before_filter :signed_in_user,
                only: [:index, :edit, :update, :destroy]
  before_filter :correct_user,   only: [:edit, :update]
  before_filter :admin_user,     only: :destroy
  before_filter :new_create_for_signed_in_user, only: [:new, :create]

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      sign_in @user
      flash[:success] = "Welcome and thanks for signing-up to #{app_name}!"
      redirect_to @user 
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @user.update_attributes(params[:user])
      flash[:success] = "Profile updated"
      sign_in @user
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted."
    redirect_to users_url
  end

  private

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user?(@user)
    end

    def admin_user
      @user = User.find(params[:id])
      redirect_to(root_path) unless current_user.admin? && !current_user?(@user)
    end

    # Attempt at Exercise 6 - new/create redirect if signed in
    def new_create_for_signed_in_user
      if signed_in?
        redirect_to root_url
      end
    end
end
CONTROLLER


    file 'sessions_controller.rb', <<-CONTROLLER
class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by_email(params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      sign_in user
      redirect_back_or user
    else
      flash.now[:error] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url 
  end
end
CONTROLLER

    insert_into_file 'application_controller.rb', :after => /protect_from_forgery\n/ do
      "  include SessionsHelper\n"
    end
  end


  # HELPERS
  inside('app/helpers') do

    # file additions
    file 'sessions_helper.rb', <<-HELPER
module SessionsHelper
  
  def sign_in(user)
    cookies.permanent[:remember_token] = user.remember_token
    self.current_user = user
  end
  
  def signed_in?
    !current_user.nil?
  end

  def current_user=(user)
    @current_user = user
  end

  def current_user
    @current_user ||= User.find_by_remember_token(cookies[:remember_token])
  end

  def current_user?(user)
    user == current_user
  end

  def signed_in_user
    unless signed_in?
      store_location
      redirect_to signin_url, notice: "Please sign in." 
    end
  end
  
  def sign_out
    self.current_user = nil
    cookies.delete(:remember_token)
  end

  def redirect_back_or(default)
    redirect_to(session[:return_to] || default)
    session.delete(:return_to)
  end

  def store_location
    session[:return_to] = request.url
  end
end

HELPER

    file 'users_helper.rb', <<-HELPER
module UsersHelper

  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user, options = { size: 50 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/\#{gravatar_id}?s=\#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
end
HELPER
    end

  # VIEWS
  inside('app/views') do
    run 'mkdir users'
    run 'mkdir sessions'

    # file additions
    insert_into_file 'layouts/_header.html.erb', :after => /<li><%= link_to "Help",    help_path %><\/li>\n/ do
      "          <% if signed_in? %>\n" +
      '            <li><%= link_to "Users", users_path %></li>' + "\n" +
      '            <li id="fat-menu" class="dropdown">' + "\n" +
      "              <a href='#' class='dropdown-toggle' data-toggle='dropdown'>\n" +
      '                Account <b class="caret"></b>' + "\n" +
      "              </a>\n" +
      "              <ul class='dropdown-menu'>\n" +
      '                <li><%= link_to "Profile", current_user %></li>' + "\n" +
      "                <li><%= link_to 'Settings', edit_user_path(current_user) %></li>\n" +
      "                <li class='divider'></li>\n" +
      "                <li>\n" +
      "                  <%= link_to 'Sign out', signout_path, method: 'delete' %>\n" +
      "                </li>\n" +
      "              </ul>\n" +
      "            </li>\n" +
      "          <% else %>\n" +
      "            <li><%= link_to 'Sign in', signin_path %></li>\n" +
      "          <% end %>\n"
    end

    insert_into_file 'static_pages/home.html.erb', :before => /<\/div>\n/ do
      "  <%= link_to 'Sign up now!', signup_path, class: 'btn btn-large btn-primary' %>\n"
    end


    file 'sessions/new.html.erb', <<-VIEW
<% provide(:title, "Sign in") %>
<h1>Sign in</h1>

<div class="row">
  <div class="span6 offset3">
    <%= form_for(:session, url: sessions_path) do |f| %>
      <%= f.label :email %>
      <%= f.text_field :email %>

      <%= f.label :password %>
      <%= f.password_field :password %>

      <%= f.submit "Sign in", class: "btn btn-large btn-primary" %>
    <% end %>

    <p>New user? <%= link_to "Sign up now!", signup_path %></p>
  </div>
</div>
VIEW

    file 'users/_fields.html.erb', <<-VIEW
<%= render 'shared/error_messages', object: f.object %>
<%= f.label :name %>
<%= f.text_field :name %>

<%= f.label :email %>
<%= f.text_field :email %>

<%= f.label :password %>
<%= f.password_field :password %>

<%= f.label :password_confirmation, "Confirm Password" %>
<%= f.password_field :password_confirmation %>
VIEW

    file 'users/_user.html.erb', <<-VIEW
<li>
  <%= gravatar_for user, size: 52 %>
  <%= link_to user.name, user %>
  <% if current_user.admin? && !current_user?(user) %>
    | <%= link_to "delete", user, method: :delete,
                                  data: { confirm: "You sure?" } %>
  <% end %>
</li>
VIEW

    file 'users/edit.html.erb', <<-VIEW
<% provide(:title, 'Edit user') %>
<h1>Update your profile</h1>
<div class="row">
  <div class="span6 offset3">
    <%= form_for(@user) do |f| %>
      <%= render 'fields', f: f %>
      <%= f.submit "Save changes", class: "btn btn-large btn-primary" %>
    <% end %>

    <%= gravatar_for @user %>
    <a href="http://gravatar.com/emails" target="_blank">change</a>
  </div>
</div>
VIEW

    file 'users/index.html.erb', <<-VIEW
<% provide(:title, "All users") %>
<h1>All users</h1>

<%= will_paginate %>

<ul class="users">
  <%= render @users %>
</ul>

<%= will_paginate %>    
VIEW

    file 'users/new.html.erb', <<-VIEW
<% provide(:title, 'Sign up') %>
<h1>Sign up</h1>
<div class="row">
  <div class="span6 offset3">
    <%= form_for(@user) do |f| %>
      <%= render 'fields', f: f %>
      <%= f.submit "Create my account", class: "btn btn-large btn-primary" %>
    <% end %>
  </div>
</div>
VIEW

    file 'users/show.html.erb', <<-VIEW
<% provide(:title, @user.name) %>
<div class="row">
  <aside class="span4">
    <section>
      <h1>
        <%= gravatar_for @user, options = { size: 40 } %>
        <%= @user.name %>
      </h1>
    </section>
  </aside>
  <div class="span8">
    <!-- microposts in tutorial went here -->
  </div>
</div>
VIEW
  end

  # ROUTES
  insert_into_file 'config/routes.rb', :after => /routes.draw do\n/ do
    "  resources :users do\n" +
    "    member do\n" +
    "    end\n" +
    "  end\n" +
    "  resources :sessions,      only: [:new, :create, :destroy]\n\n"
  end

end # Homespun Authentication BUILD COMPLETE


# Database BUILD
# ===================================================================================================================== #
  file 'config/database.example.yml', <<-DATABASE, :force => true
DATABASE

def append_sqlite3_dev
  append_file 'config/database.example.yml', <<-DATABASE
development:
  adapter: sqlite3
  database: db/development.sqlite3
  pool: 5
  timeout: 5000

DATABASE
end

def append_sqlite3_test
  append_file 'config/database.example.yml', <<-DATABASE
# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  adapter: sqlite3
  database: db/test.sqlite3
  pool: 5
  timeout: 5000

DATABASE
end

def append_sqlite3_prod
  append_file 'config/database.example.yml', <<-DATABASE
production:
  adapter: sqlite3
  database: db/production.sqlite3
  pool: 5
  timeout: 5000

DATABASE
end

def append_postgresql_dev
  append_file 'config/database.example.yml', <<-DATABASE
development:
  adapter: postgresql
  database: #{app_name}_development
  username:
  password:
  encoding: utf8
  min_messages: warning

DATABASE
end

def append_postgresql_test
  append_file 'config/database.example.yml', <<-DATABASE
test:
  adapter: postgresql
  database: #{app_name}_test
  username:
  password:
  encoding: utf8
  min_messages: warning

DATABASE
end

def append_postgresql_prod
  append_file 'config/database.example.yml', <<-DATABASE
production:
  adapter: postgresql
  database: #{app_name}_production
  username:
  password:
  encoding: utf8
  min_messages: warning

DATABASE
end

def sqlite3_dev_test
  append_file 'config/database.example.yml', <<-DATABASE
# SQLite version 3.x
#   gem install sqlite3
#
#   Ensure the SQLite 3 gem is defined in your Gemfile
#   gem 'sqlite3'  
DATABASE

  append_sqlite3_dev
  append_sqlite3_test
end

def sqlite3_all
  append_file 'config/database.example.yml', <<-DATABASE
# SQLite version 3.x
#   gem install sqlite3
#
#   Ensure the SQLite 3 gem is defined in your Gemfile
#   gem 'sqlite3'  
DATABASE

  append_sqlite3_dev
  append_sqlite3_test
  append_sqlite3_prod
end

def postgresql_prod
  # Postgresql details
  append_file 'config/database.example.yml', <<-DATABASE
DATABASE
  append_postgresql_prod
end

def postgresql_all
  # Postgresql details
  append_file 'config/database.example.yml', <<-DATABASE
DATABASE

  append_postgresql_dev
  append_postgresql_test
  append_postgresql_prod
end

def gems_add_database_for_dev_and_test
  gems = []

  if configure_app_options[:database_sqlite3_IN_dev_test] || configure_app_options[:database_sqlite3_IN_dev_test_prod]

    puts "Configuring SQLite3 database " + 
         (configure_app_options[:database_sqlite3_IN_dev_test_prod] ? "(All environments)" 
                                                                    : "(Development and Test ONLY)")
#    puts configure_app_options[:database_sqlite3_IN_dev_test_prod] ? "Configuring SQLite3 database (All environments)" : "Configuring SQLite3 database (Development and Test ONLY)"

    sqlite3_dev_test

    puts "Updating Database specific gem information for inclusion in Gemfile"
    gems << "  gem 'sqlite3', '1.3.5'"
  end

  unless gems.empty?
    gems.join("\n")
  end
end

def gems_add_database_for_prod
  gems = []

  if configure_app_options[:database_postgresql_IN_prod]
    puts "Configuring Postgresql database (Production Only)"
    postgresql_prod

    puts "Updating Database specific gem information for inclusion in Gemfile"
    gems << "group :production do
  gem 'pg', '0.12.2'
end
\n"
  end

  unless gems.empty?
    gems.join("\n")
  end
end


file 'lib/tasks/db.rake', <<-RAKE
namespace :db do
  desc 'Will completely drop the database, recreate it, then reseed and set up the test database'
  task :reset do
    %w{drop setup test:prepare}.each do |task|
      Rake::Task["db:\#{task}"].invoke
      Rake::Task['db:schema:load'].reenable
    end
     puts '== DB Reseeding Complete =='
  end
end
RAKE
    # Database BUILD COMPLETE



# ===================================================================================================================== #

#            APPLICATION FULL KICK-IN-THE-PANTS - BUNDLE INSTALL GEMS, CREATE DB, ADD INITIALIZERS + BOOTRAP IT!        #

# ===================================================================================================================== #


file 'Gemfile', <<-GEMFILE, :force => true
source 'https://rubygems.org'

gem 'rails', '3.2.11'
gem 'bootstrap-sass', '2.1'
gem 'bcrypt-ruby', '3.0.1'
gem 'faker', '1.0.1'
gem 'will_paginate', '3.0.3'
gem 'bootstrap-will_paginate', '0.0.6'
gem 'jquery-rails', '2.0.2'

group :development, :test do
#{gems_add_database_for_dev_and_test}
  gem 'rspec-rails', '2.11.0'
  gem 'guard-rspec', '1.2.1' 
  gem 'guard-spork', :github => 'guard/guard-spork'
  gem 'spork', '0.9.2'
end

# Gems used only for assets and not required
# in production environments by default.
group :assets do
  gem 'sass-rails',   '3.2.5'
  gem 'coffee-rails', '3.2.2'
  gem 'uglifier', '1.2.3'
end

group :development do
  gem 'annotate', '2.5.0'
  gem 'quiet_assets'
  gem 'better_errors'
  gem 'binding_of_caller'  
end

group :test do
  gem 'capybara', '1.1.2'
  gem 'factory_girl_rails', '4.1.0'
  gem 'cucumber-rails', '1.2.1', :require => false
  gem 'database_cleaner', '0.7.0'
  gem 'launchy', '2.1.0'
#{install_os_dependent_gems}
end

#{gems_add_database_for_prod}
GEMFILE


puts "Using RVM: #{`rvm current`.strip}"
# Use rvm for gemset mangement?
if install_options[:rvmrc]
  file '.rvmrc', <<-RVMRC
rvm use #{`rvm current`.strip}@#{app_name} --create
RVMRC

  # `rvm --rvmrc --create #{`rvm current`.strip}@#{app_name}`
  # use doesn't seem to work in scripts as creates child shell/process
  # Had wanted to use the .rvmrc file generated & switch on the bundler section
  sed_start = `grep -n '# If you use bundler, this might be useful to you:' .rvmrc | cut -f1 -d:`
  sed_end = `grep -n "bundle install | GREP_OPTIONS= \\grep -vE '^Using|Your bundle is complete'" .rvmrc`
  puts "#{sed_start}"
  puts "#{sed_end}"
  #sed_end +=
  #puts "#{sed_end}"

  require 'rvm'
  @env = RVM::Environment.new( "#{`rvm current`.strip}" )
  @env.gemset_create("#{app_name}")
  @env.gemset_use!("#{app_name}")

  run('rvm current')
end

run 'bundle install'

# Remove Test::Unit (if generated)
FileUtils.rm_rf('test')

generate('rspec:install')
file '.rspec', <<-RSPEC, force: true
  --colour
  --drb
RSPEC


# run 'cp config/database.example.yml config/database.yml'
#run_additional_rake_tasks
if configure_app_options[:database_sqlite3] || configure_app_options[:database_postgresql]
  #run 'bundle install'
  run 'bundle exec rake db:create --trace' 
  run 'bundle exec rake db:migrate --trace'
  run 'rake db:test:prepare'
else
  run 'rm config/database.example.yml'
  puts "DATABASE: Not created - You will need to install & configure a flavour of your choice! Enjoy!"
end



# Initializers



# Add RSpec tests BUILD
# ===================================================================================================================== #
if configure_app_options[:rspec_base]
  puts "Adding RSpec base build"

  inside('spec') do
    run 'mkdir requests'
    run 'mkdir support'
    run 'mkdir helpers'
  end

  if configure_app_options[:static_pages]
    inside('spec/requests') do
      file 'static_pages_spec.rb', <<-SPEC_VIEW
require 'spec_helper'

describe "StaticPages" do

  subject { page }

  shared_examples_for "all static pages" do
    it { should have_selector('h1',    text: heading) }
    it { should have_selector('title', text: full_title(page_title)) }
  end

  describe "Home page" do
    before { visit root_path }

    let(:heading) { '#{app_name}' }
    let(:page_title) { '' }

    it_should_behave_like "all static pages"
    it { should_not have_selector 'title', text: '| Home' }

  end

  describe "Help page" do
    before { visit  help_path }

    let(:heading) { 'Help' }
    let(:page_title) { 'Help' }

    it_should_behave_like "all static pages"
  end

  describe "About page" do
    before { visit  about_path }

    let(:heading) { 'About Us' }
    let(:page_title) { 'About Us' }

    it_should_behave_like "all static pages"
  end

  describe "Contact page" do
    before { visit  contact_path }

    let(:heading) { 'Contact' }
    let(:page_title) { 'Contact' }

    it_should_behave_like "all static pages"
  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    page.should have_selector 'title', text: full_title('About Us')
    click_link "Help"
    page.should have_selector 'title', text: full_title('Help')
    click_link "Contact"
    page.should have_selector 'title', text: full_title('Contact')
    click_link "Home"
    click_link "#{app_name}"
    page.should have_selector 'title', text: full_title('')
  end
end

SPEC_VIEW
    end
  end

  if configure_app_options[:base_title_helper]
    inside('spec/helpers') do
      file 'application_helper_spec.rb', <<-SPEC_HELPER
require 'spec_helper'

describe ApplicationHelper do

  describe "full_title" do
    it "should include the page title" do
      full_title("foo").should =~ /foo/ 
    end

    it "should include the base title" do
      full_title("foo").should =~ /^#{app_name}/
    end

    it "should not include a bar for the home page" do
      full_title('').should_not =~ /\\|/
    end
  end
end      
SPEC_HELPER
    end
  end

  if configure_app_options[:error_messages_shared_view]
    inside('spec/support') do
      file 'utilities.rb', <<-SPEC_SUPPORT
include ApplicationHelper

RSpec::Matchers.define :have_error_message do | message |
  match do |page|
    page.should have_selector('div.alert.alert-error', text: message)
  end
end      
SPEC_SUPPORT
    end
  end

#========



#========
  if configure_app_options[:homespun_authentication]
    # SPEC FACTORIES
    inside('spec') do
      file 'factories.rb', <<-FACTORY
FactoryGirl.define do
  factory :user do
    sequence(:name)        { |n| "Person \#{n}" }
    sequence(:email)       { |n| "person_\#{n}@example.com" }
    password               "foobar"
    password_confirmation  "foobar"

    factory :admin do
      admin true
    end
  end
end
FACTORY
    end

    # SPEC MODELS
    inside('spec/models') do
      file 'user_spec.rb', <<-SPEC_MODEL
# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  email      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'spec_helper'

describe User do
  before { @user = User.new(name: "Example User", 
                            email: "user@example.com",
                            password: "foobar",
                            password_confirmation: "foobar" 
                           ) }

  subject { @user }

  it { should respond_to(:name) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:remember_token) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:admin) }

  it { should be_valid }

  describe "accessible attributes" do
    it "should not allow access to admin" do
      expect do
        User.new(name: "disallowed_user",
                 email: "disallowed@example.com",
                 password: "foobar",
                 password_confirmation: "foobar",
                 admin: false
                )
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  describe "when name is not present" do
    before { @user.name = " " }
    it { should_not be_valid }
  end

  describe "when name is too long" do
    before { @user.name = "a" * 51 }
    it { should_not be_valid }
  end

  describe "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end

  describe "when password is not present" do
    before { @user.password = @user.password_confirmation = " " }
    it { should_not be_valid }
  end

  describe "when password doesn't match confirmation" do
    before { @user.password_confirmation = "mismatch" }
    it { should_not be_valid }
  end
 
  describe "when password confirmation is nil" do
    before { @user.password_confirmation = nil }
    it { should_not be_valid }
  end
 
  describe "with a password that's too short" do
    before { @user.password = @user.password_confirmation = "a" * 5 }
    it { should be_invalid }
  end
 
  describe "return value of authenticate method" do
    before { @user.save }
    let(:found_user) { User.find_by_email(@user.email) }

    describe "with valid password" do
      it { should == found_user.authenticate(@user.password) }
    end

    describe "with invalid password" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid") }
      
      it { should_not == user_for_invalid_password }
      specify { user_for_invalid_password.should be_false }
    end
  end

  describe "remember token" do
    before { @user.save }
    its(:remember_token) { should_not be_blank }
  end

  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        @user.should_not be_valid
      end
    end
  end

  describe "when email format is valid" do
    it "should be invalid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.last@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @user.email = valid_address
        @user.should be_valid 
      end
    end
  end

  describe "when email address is already taken" do
    before do 
      user_with_same_email = @user.dup
      user_with_same_email.email = @user.email.upcase
      user_with_same_email.save
    end
    it { should_not be_valid }
  end

  describe "when email address with mixed case" do
    let(:mixed_case_email) { "Foo@ExAMPle.COM" }

    it "should be saved as all lower-case" do
      @user.email = mixed_case_email
      @user.save
      @user.reload.email.should == mixed_case_email.downcase
    end
  end

  describe "with admin attribute set to 'true'" do
    before do
      @user.save!
      @user.toggle!(:admin)
    end

    it { should be_admin }
  end

end
SPEC_MODEL
    end
  end

    # SPEC REQUESTS
    inside('spec/requests') do
      file 'authentication_pages_spec.rb', <<-SPEC_REQUEST
require 'spec_helper'

describe "AuthenticationPages" do

  subject { page }

  describe "signin page" do
    before { visit signin_path }

    it { should have_selector('h1',    text: 'Sign in') }
    it { should have_selector('title', text: 'Sign in') }
  end

  describe "signin" do
    before { visit signin_path }

    describe "with invalid information" do
      before { click_button "Sign in" }

      it { should have_selector('title', text: 'Sign in') }
      it { should have_error_message('Invalid') }

      it { should_not have_link('Users',       href: users_path) }
      it { should_not have_link('Profile') }
      it { should_not have_link('Settings') }
      it { should_not have_link('Sign out',    href: signout_path) }

      describe "after vising another page" do
        before { click_link "Home" }
        it { should_not have_error_message('Invalid') }
      end
    end

    describe "with valid information" do
      let(:user) { FactoryGirl.create(:user) }
      before { sign_in user }

      it { should have_selector('title',   text: user.name) }

      it { should have_link('Users',       href: users_path) }
      it { should have_link('Profile',     href: user_path(user)) }
      it { should have_link('Settings',    href: edit_user_path(user)) }
      it { should have_link('Sign out',    href: signout_path) }

      it { should_not have_link('Sign in', href: signin_path) }

      describe "followed by signout" do
        before {click_link "Sign out" }
        it { should have_link('Sign in') }
      end
    end
  end

  describe "authorization" do

    describe "for non-signed in users" do
      let(:user) { FactoryGirl.create(:user) }

      describe "when attempting to visit a protected page" do
        before do
          visit edit_user_path(user) 
          fill_in "Email",    with: user.email
          fill_in "Password", with: user.password
          click_button "Sign in"
        end

        describe "after signing in" do
          it "should render the desired protected page" do
            page.should have_selector('title', text: 'Edit user')
          end 

          describe "when signing in again" do
            before do
              delete signout_path
              visit signin_path
              fill_in "Email",    with: user.email
              fill_in "Password", with: user.password
              click_button "Sign in"
            end
            
            it "should render the default (profile) page" do
              page.should have_selector('title', text: user.name) 
            end
          end
        end
      end

      #describe "visiting the edit page" do
        #before { visit edit_user_path(user) }
        #it { should have_selector('title', text: 'Sign in') }
      #end

      #describe "submitting to the update action" do
        #before { put user_path(user) }
        #specify { response.should redirect_to(signin_path) }
      #end

      describe "in the Users controller" do

        describe "visiting the user index" do
          before { visit users_path }
          it { should have_selector('title', text: 'Sign in') }
        end
      end
    end

    describe "as wrong user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:wrong_user) { FactoryGirl.create(:user, email: "wrong@example.com") }
      before { sign_in user }
      
      describe "visiting Users#edit page" do
        before { visit edit_user_path(wrong_user) }
        it { should_not have_selector('title', text: full_title('Edit user')) } 
      end

      describe "submitting a PUT request to the Users#update action" do
        before { put user_path(wrong_user) }
        specify { response.should redirect_to(root_path) }
      end
    end

    describe "as non-admin user" do
      let(:user) { FactoryGirl.create(:user) }
      let(:non_admin) { FactoryGirl.create(:user) }

      before { sign_in non_admin }

      describe "submitting a DELETE request to the Users#destroy action" do
        before { delete user_path(user) }
        specify { response.should redirect_to(root_path) }
      end

      describe "submitting a GET request to the Users#new action" do
        before { get new_user_path }
        specify { response.should redirect_to(root_url) }
      end

      describe "submitting a POST request to the Users#create action" do
        before { post users_path }
        specify { response.should redirect_to(root_url) }
      end
    end
  end
end

      SPEC_REQUEST



      file 'user_pages_spec.rb', <<-SPEC_REQUEST

require 'spec_helper'

describe "User Pages" do

  subject { page }

  describe "index" do

    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in user
      visit users_path
    end

    it { should have_selector('title', text: 'All users') }
    it { should have_selector('h1',    text: 'All users') }

    describe "pagination" do

      before(:all) { 30.times { FactoryGirl.create(:user) } }
      after(:all)  { User.delete_all }

      it { should have_selector('div.pagination') }

      it "should list each user" do
        User.paginate(page: 1).each do |user|
          page.should have_selector('li', text: user.name)
        end
      end
    end

    describe "delete links" do

      it { should_not have_link('delete') }

      describe "as an admin user" do
        let(:admin) { FactoryGirl.create(:admin) }
        before do
          sign_in admin
          visit users_path
        end

        it { should have_link('delete', href: user_path(User.first)) }
        it "should be able to delete another user" do
          expect { click_link('delete') }.to change(User, :count).by(-1)
        end
        it { should_not have_link('delete', href: user_path(admin)) }

        it " should not be able to delete oneself" do
          expect { delete user_path(admin) }.not_to change(User, :count).by(-1)
        end
      end
    end
  end

  describe "signup page" do
    before { visit signup_path }

    it { should have_selector('h1',        text: 'Sign up') }
    it { should have_selector 'title',     text: full_title('Sign up') }
  end

  describe "signup" do

    before { visit signup_path }

    let(:submit) { "Create my account" }

    describe "with invalid information" do
      it "should not create a user" do
        expect { click_button submit }.not_to change(User, :count)
      end

      describe "after submission" do
        before { click_button submit }
        
        it { should have_selector('title', text: 'Sign up') }
        it { should have_content('error') }
      end
    end

    describe "with valid information" do
      before do
        fill_in "Name",             with: "Example User"
        fill_in "Email",            with: "user@example.com"
        fill_in "Password",         with: "foobar"
        fill_in "Confirm Password", with: "foobar"
      end

      it "should create a user" do
        expect { click_button submit }.to change(User, :count).by(1)
      end

      describe "after saving the user" do
        before { click_button submit }
        let(:user) { User.find_by_email('user@example.com') }

        it { should have_selector('title', text: user.name) }
        it { should have_selector('div.alert.alert-success', text: 'Welcome') }
        it { should have_link('Sign out') }
      end
    end
  end

  describe "profile page" do
    let(:user) { FactoryGirl.create(:user) } 
    before { visit user_path(user) }

    it { should have_selector('h1',        text: user.name) }
    it { should have_selector('title',     text: user.name) }
  end

  describe "edit" do
    let(:user) { FactoryGirl.create(:user) }
    before do
      sign_in user
      visit edit_user_path(user) 
    end

    describe "page" do
      it { should have_selector('h1',    text: "Update your profile") }
      it { should have_selector('title', text: "Edit user") }
      it { should have_link('change', href: "http://gravatar.com/emails") }
    end

    describe "with invalid information" do
      before { click_button "Save changes" }
      
      it { should have_content('error') }
    end

    describe "with valid information" do
      let(:new_name) { "New Name" }
      let(:new_email) { "new@example.com" }
      before do 
        fill_in "Name",             with: new_name
        fill_in "Email",            with: new_email
        fill_in "Password",         with: user.password
        fill_in "Confirm Password", with: user.password
        click_button "Save changes" 
      end
 
      it { should have_selector('title', text: new_name) } 
      it { should have_selector('div.alert.alert-success') } 
      it { should have_link('Sign out', href: signout_path) } 
      specify { user.reload.name.should == new_name }
      specify { user.reload.email.should == new_email }
    end
  end

end
SPEC_REQUEST
    end

    # Spec Utilities
    inside('spec/support') do
      insert_into_file 'utilities.rb', :after => /include ApplicationHelper\n\n/ do
        "def valid_signin(user)\n" +
        "  fill_in 'Email',    with: user.email\n" + 
        "  fill_in 'Password', with: user.password\n" +
        "  click_button 'Sign in'\n" +
        "end\n" +
        "\n" +
        "def sign_in(user)\n" +
        "  visit signin_path\n" +
        "  fill_in 'Email',    with: user.email\n" + 
        "  fill_in 'Password', with: user.password\n" +
        "  click_button 'Sign in'\n" +
        "  # Sign in when not using Capybara as well.\n" +
        "  cookies[:remember_token] = user.remember_token\n" +
        "end\n\n"
      end
    end  
end


# Finalise

run 'rm public/index.html'
run 'rm README.rdoc'

file 'public/humans.txt', <<-FILE
/* TEAM */
  This app was built by RubyN00b
  Twitter: @dockyard
  Email: digital.learning.journeys@gmail.com
  Location: London, UK

/* SITE */
  Standards: HTML5, CSS3
  Components: Ruby on Rails
  Software: Vim, OSX
  Frameworks: Ruby on Rails, jQuery, Bootstrap
FILE

file 'README.md', <<-README
# #{app_name.titleize} #

## Getting set up ##

This app uses [rvm](http://rvm.io)

Please follow the instructions for setting on rvm if you already have not done so.

Install the app:

```
bundle install
```

Configure, create, migrate, and seed the database

```
cp config/database.yml/example config/database.yml
vim config/database.yml
rake db:reseed
```

Happy Days are here!
README

# Git Ignore
file '.gitignore', <<-GITIGNORE, :force => true
.bundle
.DS_Store
*.swp
*.swo
**/.DS_STORE
bin/*
binstubs/*
bundler_stubs/*
config/database.yml
coverage/*
db/*.sqlite3
db/structure.sql
log/*.log
log/*.pid
public/system/*
public/stylesheets/compiled/*
public/assets/*
public/uploads/*
tmp/*
GITIGNORE

# get_rid_of_shitty_double_quotes

git :init
git :add => '.'
git :commit => "-a -m 'Initial Project Generated from Railify'"


puts "Attempting to confirm build clean by running Tests"
run 'bundle exec rspec spec'
exit

# END OF EXISTING TEMPLATE

# Testing additions (RSpec)

inside('spec') do
  run 'mkdir config'
  run 'mkdir -p requests/step_helpers'
  FileUtils.touch('requests/step_helpers/.gitkeep')

  insert_into_file 'spec_helper.rb', :before => %{Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}\n} do
    "Dir[Rails.root.join('spec/config/**/*.rb')].each {|f| require f}\n" +
    "Dir[Rails.root.join('spec/requests/step_helpers/**/*.rb')].each {|f| require f}\n"
  end
  replace_line('spec_helper.rb', :match => /config.use_transactional_fixtures = true/, :with => '  config.use_transactional_fixtures = false')
end

file 'spec/support/factories.rb', <<-FILE
# Factories

FILE

inside('spec/config') do
  file 'rspec.rb', <<-FILE
RSpec.configure do |config|
  config.treat_symbols_as_metadata_keys_with_true_values = true
end
FILE

  file 'factory_girl.rb', <<-FILE
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
end
FILE

  file 'database_cleaner.rb', <<-FILE
require 'database_cleaner'

RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.clean
    DatabaseCleaner.strategy = :deletion
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end
FILE

  file 'capybara.rb', <<-FILE
require 'capybara/rspec'
FILE

  file 'capybara-email.rb', <<-FILE
require 'capybara/email/rspec'
FILE

end


